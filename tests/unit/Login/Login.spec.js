import { mount } from "@vue/test-utils"
import Login from "@/views/Login/Login.vue"
import { createStore } from "vuex"
import { LOGGING_IN } from "@/store/actions"
import { LOG_IN, SET_USER } from "@/store/mutations.js"
import * as request from "@/api/request";

jest.mock('@/api/request', () => ({
    login: jest.fn(),
    register: jest.fn()
}))

describe("Login.vue", () => {
    const store = createStore({
        state: {
            login: false,
            user: null
        },
        mutations: {
            [LOG_IN](state) {
                state.login = true;
            },
            [SET_USER](state, payload) {
                state.user = payload.payload;
                //payload => user info
            },
        },
        actions: {
            async [LOGGING_IN]({ commit }, payload) {
                const { email, password, loginMode } = payload;
                return new Promise((resolve, reject) => {
                    if (loginMode) {
                        request.login(email, password, (error, data) => {
                            if (error) {
                                reject(error);
                            } else {
                                console.log(data);
                                resolve(data.user.email);
                                commit(LOG_IN);
                                commit({ type: SET_USER, payload: data.user.email });
                            }
                        });
                    } else {
                        request.register(email, password, (error, data) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(data.user.email);
                                commit(LOG_IN);
                                commit({
                                    type: SET_USER,
                                    payload: data.user.email,
                                });
                            }
                        });
                    }
                });
            },
        }
    })
    const wrapper = mount(Login, {
        global: {
            plugins: [store]
        }
    })
    
    it("shows form login", () => {
        expect(wrapper.html()).toContain('form')
    })
})
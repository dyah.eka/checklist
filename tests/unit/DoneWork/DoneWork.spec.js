import { mount } from "@vue/test-utils"
import DoneWork from "@/views/DoneWork/DoneWork.vue"
import { createStore } from "vuex"
import { CHECKLING_CHECKLIST, DELETING_CHECKLIST } from "@/store/actions"
import { CHECK_CHECKLIST, DELETE_CHECKLIST } from "@/store/mutations.js"

describe("DoneWork.vue", () => {
    const store = createStore({
        state: {
            checklistData: [
                {
                    id: 1,
                    checklist: 'Add some nuts',
                    date: '2020/01/01',
                    done: true
                }
            ]
        },
        getters: {
            doneChecklist: state => state.checklistData.filter(check => check.done)
        },
        mutations: {
            [DELETE_CHECKLIST](state, payload) {
                const checks = state.checklistData.filter(
                    (check) => check.id !== payload.payload
                    //payload => id
                );
                state.checklistData = checks;
            },
            [CHECK_CHECKLIST](state, payload) {
                const checks = state.checklistData.find(
                    (check) => check.id === payload.payload
                    //payload => id
                );
                checks.done = !checks.done;
            },
        },
        actions: {
            [DELETING_CHECKLIST]({ commit }, payload) {
                commit({
                    type: DELETE_CHECKLIST,
                    payload: payload.payload,
                });
            },
            [CHECKLING_CHECKLIST]({ commit }, payload) {
                commit({
                    type: CHECK_CHECKLIST,
                    payload: payload.payload,
                });
            },
        }
    })
    const wrapper = mount(DoneWork, {
        global: {
            plugins: [store]
        }
    })
    
    it("shows checklist data", () => {
        expect(wrapper.html()).toContain('Add some nuts')
    })
    it("check a checklist to be undone", async () => {
        const doneButton = wrapper.get('button.bg-blue-300')

        expect(wrapper.html()).toContain('Add some nuts')

        await doneButton.trigger('click')
        
        expect(wrapper.html()).not.toContain('Add some nuts')
        expect(store.state.checklistData.length).toBe(1)
    })
    it("delete checklist when delete button clicked", async () => {
        await store.dispatch(CHECKLING_CHECKLIST, {payload: 1})
        
        const deleteButton = wrapper.get('button.bg-red-500')

        expect(wrapper.html()).toContain('Add some nuts')

        await deleteButton.trigger('click')
        
        expect(wrapper.html()).not.toContain('Add some nuts')
        expect(store.state.checklistData.length).toBe(0)
    })
})
import { mount } from "@vue/test-utils"
import ChecklistForm from "@/views/ChecklistForm/ChecklistForm.vue"
import { createStore } from "vuex"
import { ADDING_CHECKLIST } from "@/store/actions"
import { ADD_CHECKLIST } from "@/store/mutations.js"

describe("ChecklistForm.vue", () => {
    const store = createStore({
        state: {
            checklistData: [],
            user: {
                uid: '12345'
            }
        },
        mutations: {
            [ADD_CHECKLIST](state, payload) {
                state.checklistData.push(payload.payload.checklist);
                //payload => checklist
                state.checklistData.sort(
                    (a, b) => Date.parse(a.date) - Date.parse(b.date)
                );
            },
        },
        actions: {
            [ADDING_CHECKLIST]({ commit }, payload) {
                commit({
                    type: ADD_CHECKLIST,
                    payload: payload.payload,
                });
            },
        }
    })
    const wrapper = mount(ChecklistForm, {
        global: {
            plugins: [store]
        }
    })

    it("shows the form", () => {
        expect(wrapper.html()).toContain('form')
    })
    it("adds two checklist to store", async () => {
        await wrapper.get('input[type="text"]').setValue("Todo")
        await wrapper.get('input[type="date"]').setValue("2020-02-02")
        await wrapper.get('input[type="checkbox"').setValue(true)

        await wrapper.get('form').trigger('submit')

        expect(store.state.checklistData.length).toBe(1)

        await wrapper.get('input[type="text"]').setValue("Todo a list")
        await wrapper.get('input[type="date"]').setValue("2020-02-02")
        await wrapper.get('input[type="checkbox"').setValue(false)

        await wrapper.get('form').trigger('submit')

        expect(store.state.checklistData.length).toBe(2)
    })
    it("shows an error if checklist empty and/or date format is wrong", async () => {
        await wrapper.get('input[type="text"]').setValue("Todo a list")
        await wrapper.get('input[type="date"]').setValue("02/02/2012")
        await wrapper.get('input[type="checkbox"').setValue(false)

        await wrapper.get('form').trigger('submit')

        expect(wrapper.html()).toContain("Checklist mustn't empty and/or date format are wrong")

        await wrapper.get('input[type="text"]').setValue("")
        await wrapper.get('input[type="date"]').setValue("02/02/2012")
        await wrapper.get('input[type="checkbox"').setValue(false)

        await wrapper.get('form').trigger('submit')

        expect(wrapper.html()).toContain("Checklist mustn't empty and/or date format are wrong")

        await wrapper.get('input[type="text"]').setValue("")
        await wrapper.get('input[type="date"]').setValue("")
        await wrapper.get('input[type="checkbox"').setValue(false)

        await wrapper.get('form').trigger('submit')

        expect(wrapper.html()).toContain("Checklist mustn't empty and/or date format are wrong")
    })
    it("error message dissapear if inputs added to the store", async () => {
        await wrapper.get('input[type="text"]').setValue("")

        await wrapper.get('form').trigger('submit')

        expect(wrapper.html()).toContain("Checklist mustn't empty and/or date format are wrong")

        await wrapper.get('input[type="text"]').setValue("Todo a list")
        await wrapper.get('input[type="date"]').setValue("2020-02-02")
        await wrapper.get('input[type="checkbox"').setValue(false)

        await wrapper.get('form').trigger('submit')

        expect(wrapper.html()).not.toContain("Checklist mustn't empty and/or date format are wrong")
    })
})
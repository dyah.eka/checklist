import firebase from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
	apiKey: "AIzaSyAgCu6mvR1PnWPgE3fTnAuqkqBsmO2jBck",
	authDomain: "checklist-todos.firebaseapp.com",
	projectId: "checklist-todos",
	storageBucket: "checklist-todos.appspot.com",
	messagingSenderId: "230877470916",
	appId: "1:230877470916:web:8df7abcc21c419dfee2991",
	measurementId: "G-JP2NQBH0RK",
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

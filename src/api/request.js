import { auth } from "./firebase";

const authErrorMessage = (errorCode) => {
    switch (errorCode) {
        case "auth/invalid-email":
            return "Invalid email address.";
        case "auth/weak-password":
            return "Password is too weak.";
        default:
            return "The email or password is invalid";
    }
}

export const register = (email, password, callback) => {
    auth.createUserWithEmailAndPassword(email, password)
        .then(userCredential => {
            callback(null, userCredential);
        })
        .catch(error => {
            callback(authErrorMessage(error.code), null);
        }) 
}

export const login = (email, password, callback) => {
    auth.signInWithEmailAndPassword(email, password)
        .then(userCredential => {
            callback(null, userCredential)
        })
        .catch(error => {
            callback(authErrorMessage(error.code), null)
        })
}
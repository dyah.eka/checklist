export const ADD_CHECKLIST = "ADD_CHECKLIST";
export const DELETE_CHECKLIST = "DELETE_CHECKLIST";
export const CHECK_CHECKLIST = "CHECK_CHECKLIST";

export const SET_USER = "SET_USER";

export const LOG_IN = "LOG_IN";
export const LOG_OUT = "LOG_OUT";

// import { reactive } from "vue";

// const store = {
// 	state: reactive({
//         login: false,
// 		checklistData: [],
// 	}),
// addChecklist(checklist) {
// 	store.state.checklistData.push(checklist);
// 	store.state.checklistData.sort(
// 		(a, b) => Date.parse(a.date) - Date.parse(b.date)
// 	);
// },
// deleteChecklist(id) {
// 	const checks = store.state.checklistData.filter((check) => check.id !== id);
// 	store.state.checklistData = checks;
// },
// };

// export default store;

import { createStore } from "vuex";
import { login, register } from "../api/request";
import {
	ADDING_CHECKLIST,
	CHECKLING_CHECKLIST,
	DELETING_CHECKLIST,
	LOGGING_IN,
	LOGGING_OUT,
} from "./actions";
import {
	ADD_CHECKLIST,
	CHECK_CHECKLIST,
	DELETE_CHECKLIST,
	LOG_IN,
	LOG_OUT,
	SET_USER,
} from "./mutations";

const store = createStore({
	state() {
		return {
			user: null,
			login: false,
			checklistData: [],
		};
	},
	getters: {
		doneChecklist(state) {
			if (state.checklistData.length !== 0) {
				return state.checklistData.filter((check) => check.done);
			}
			return state.checklistData;
		},
		notDoneChecklist(state) {
			if (state.checklistData.length !== 0) {
				return state.checklistData.filter((check) => !check.done);
			}
			return state.checklistData;
		},
	},
	mutations: {
		[ADD_CHECKLIST](state, payload) {
			state.checklistData.push(payload.payload.checklist);
			//payload => checklist
			state.checklistData.sort(
				(a, b) => Date.parse(a.date) - Date.parse(b.date)
			);
		},
		[DELETE_CHECKLIST](state, payload) {
			const checks = state.checklistData.filter(
				(check) => check.id !== payload.payload
				//payload => id
			);
			state.checklistData = checks;
		},
		[CHECK_CHECKLIST](state, payload) {
			const checks = state.checklistData.find(
				(check) => check.id === payload.payload
				//payload => id
			);
			checks.done = !checks.done;
		},
		[SET_USER](state, payload) {
			state.user = payload.payload;
			//payload => user info
		},
		[LOG_IN](state) {
			state.login = true;
		},
		[LOG_OUT](state) {
			state.login = false;
		},
	},
	actions: {
		[ADDING_CHECKLIST]({ commit }, payload) {
			commit({
				type: ADD_CHECKLIST,
				payload: payload.payload,
			});
		},
		[DELETING_CHECKLIST]({ commit }, payload) {
			commit({
				type: DELETE_CHECKLIST,
				payload: payload.payload,
			});
		},
		[CHECKLING_CHECKLIST]({ commit }, payload) {
			commit({
				type: CHECK_CHECKLIST,
				payload: payload.payload,
			});
		},
		async [LOGGING_IN]({ commit }, payload) {
			const { email, password, loginMode } = payload;
			return new Promise((resolve, reject) => {
				if (loginMode) {
					login(email, password, (error, data) => {
						if (error) {
							reject(error);
						} else {
							console.log(data);
							resolve(data.user.email);
							commit(LOG_IN);
							commit({ type: SET_USER, payload: data.user.email });
						}
					});
				} else {
					register(email, password, (error, data) => {
						if (error) {
							reject(error);
						} else {
							resolve(data.user.email);
							commit(LOG_IN);
							commit({
								type: SET_USER,
								payload: data.user.email,
							});
						}
					});
				}
			});
		},
		[LOGGING_OUT]({ commit }) {
			commit(LOG_OUT);
			commit({ type: SET_USER, payload: null });
		},
	},
});

export default store;

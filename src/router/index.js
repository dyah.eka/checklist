import { createRouter, createWebHistory } from "vue-router";
import ChecklistForm from "../views/ChecklistForm/ChecklistForm.vue";

import store from "../store/store";

const ToDo = () => import("../views/ToDo/Todo.vue");
const DoneWork = () => import("../views/DoneWork/DoneWork.vue");
const Login = () => import("../views/Login/Login.vue");
const Logout = () => import("../views/Logout/Logout.vue");

const routes = [
	{
		path: "/add",
		name: "home",
		component: ChecklistForm,
	},
	{
		path: "/to-do",
		name: "ToDo",
		component: ToDo,
	},
	{
		path: "/done",
		name: "DoneWork",
		component: DoneWork,
	},
	{
		path: "/login",
		name: "Login",
		component: Login,
	},
	{
		path: "/logout",
		name: "Logout",
		component: Logout,
	},
	{
		path: "/:patchAll(.*)",
		redirect: "/login",
	},
];

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
});

router.beforeEach((to, from, next) => {
	if (to.name !== "Login" && !store.state.login) next({ name: "Login" });
	else if (to.name == "Login" && store.state.login) next({ name: "home" });
	else next();
});

export default router;
